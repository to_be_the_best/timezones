#!/usr/bin/python

import util
from person import Person
import database


def edit_person(person: Person, time_difference: str):
    time_difference_float = util.time_difference_to_float(time_difference)

    person.timezone = time_difference
    person.timezone_float = time_difference_float

    database.edit_person()
