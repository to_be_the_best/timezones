# Timezones Bot

## Install

`pip install -r requirements.txt`

Copy `chatwars.db.dist` to `chatwars.db`.

Copy `.env.dist` to `.env`.

Edit the token to your own token.

Add channel id's to LIST_OF_CHANNELS.

Add user id's to LIST_OF_USERS.


