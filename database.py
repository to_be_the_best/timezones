#!/usr/bin/python

from typing import Optional, List
from sqlalchemy.orm.query import Query
from database_connection import session
from person import Person
from timezone import Timezone


def list_timezones() -> List[Timezone]:
    query: Query = session.query(Person).group_by(Person.timezone_float).order_by(Person.timezone_float)
    query: Query = query.with_entities(Person.timezone_float, Person.timezone)
    results: list = query.all()
    timezones: List[Timezone] = []
    for result in results:
        timezones.append(Timezone(result[0], result[1]))
    return timezones


def list_persons() -> List[Person]:
    return session.query(Person).order_by(Person.timezone_float).order_by(Person.name).all()


def list_persons_by_timezone(timezone: float) -> List[Person]:
    return session.query(Person).filter(Person.timezone_float == timezone).order_by(Person.name).all()


def get_person(identifier: int) -> Optional[Person]:
    return session.query(Person).filter(Person.id == identifier).first()


def add_person(person: Person):
    session.add(person)
    session.commit()


def edit_person():
    session.commit()


def edit_person_test():
    foo = session.query(Person).order_by(Person.timezone_float).order_by(Person.name).first()
    foo.name = 'Tom Reinders lala'
    session.commit()


def remove_person(identifier: int):
    person = session.query(Person).filter(Person.id == identifier).first()
    session.delete(person)
    session.commit()
