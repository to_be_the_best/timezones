#!/usr/bin/python

from functools import wraps
import telegram
import settings


def restricted(func):
    @wraps(func)
    def wrapped(bot: telegram.Bot, update: telegram.Update, *args, **kwargs):
        chat_id = update.message.chat.id
        user_id = update.effective_user.id
        if chat_id not in settings.LIST_OF_CHANNELS and user_id not in settings.LIST_OF_USERS:
            print("Unauthorized access denied for {} in chat {}.".format(user_id, chat_id))
            bot.send_message(chat_id=update.message.chat_id, text='Access Denied')
            return
        return func(bot, update, *args, **kwargs)
    return wrapped
