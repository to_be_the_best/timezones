#!/usr/bin/python

import telegram
import util
from person import Person
import database


def add_person(user: telegram.User, time_difference: str, name: str):
    time_difference_float = util.time_difference_to_float(time_difference)

    if name == '':
        name = user.full_name

    person = Person(
        id=user.id,
        name=name,
        username=user.username,
        timezone=time_difference,
        timezone_float=time_difference_float
    )

    database.add_person(person)
