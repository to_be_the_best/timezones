#!/usr/bin/python


class Timezone:
    def __init__(self, timezone_float: float, timezone: str):
        self.timezone_float = timezone_float
        self.timezone = timezone
