#!/usr/bin/python

from datetime import datetime, timezone, timedelta
from typing import Optional, List


def time_difference_to_float(string: str) -> float:
    hour = string
    minute = None

    if ':' in string:
        hour, minute = string.split(':')
        minute = float(minute) / 60

    hour = float(hour)

    if minute:
        hour = hour + minute

    return hour


def float_to_local_datetime(timezone_float: float) -> datetime:
    local_delta = timedelta(hours=timezone_float)
    local_timezone = timezone(local_delta)
    local_datetime = datetime.now(local_timezone)

    return local_datetime


def string_to_ids(ids_string: str) -> List[Optional[int]]:
    if ids_string == '':
        return list()

    ids = ids_string.split(',')

    return list(map(int, ids))


def get_part_of_day(local_datetime: datetime) -> str:
    hour = local_datetime.hour

    return (
        "🌅" if 6 <= hour <= 11
        else
        "🌇" if 12 <= hour <= 17
        else
        "🏙" if 18 <= hour <= 23
        else
        "🌃"
    )
