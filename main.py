#!/usr/bin/python

import logging
from telegram.ext import CommandHandler
from telegram.ext import Updater
import settings
from command.display import display
from command.add import add
from command.add_admin import add_admin
from command.edit import edit
from command.edit_admin import edit_admin

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

updater = Updater(token=settings.TOKEN)
dispatcher = updater.dispatcher

dispatcher.add_handler(CommandHandler('display', display, pass_args=False))

dispatcher.add_handler(CommandHandler('add', add, pass_args=True))

dispatcher.add_handler(CommandHandler('add_admin', add_admin, pass_args=True))

dispatcher.add_handler(CommandHandler('edit', edit, pass_args=True))

dispatcher.add_handler(CommandHandler('edit_admin', edit_admin, pass_args=True))

print('Starting polling...')
updater.start_polling()
print('Polling started')
