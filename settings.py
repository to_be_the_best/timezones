#!/usr/bin/python

import os
import sys
from dotenv import load_dotenv
from util import string_to_ids

load_dotenv()

try:
    TOKEN = os.environ['TOKEN']
except KeyError:
    print('Please set the environment variable TOKEN')
    sys.exit(1)

try:
    channels_string = os.environ['LIST_OF_CHANNELS']

    LIST_OF_CHANNELS = string_to_ids(channels_string)
except KeyError:
    print('Please set the environment variable LIST_OF_CHANNELS')
    sys.exit(1)

try:
    users_string = os.environ['LIST_OF_USERS']

    LIST_OF_USERS = string_to_ids(users_string)
except KeyError:
    print('Please set the environment variable LIST_OF_USERS')
    sys.exit(1)
