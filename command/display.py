#!/usr/bin/python

import telegram
from datetime import datetime
import database
import util
from restricted import restricted


@restricted
def display(bot: telegram.Bot, update: telegram.Update):
    now = datetime.utcnow()

    time_string = now.strftime("%Y-%m-%d %H:%M:%S")

    timezones = database.list_timezones()

    response = '<b>Current time:</b>\n'
    response += 'UTC: ' + time_string + '\n\n'

    response += '<b>Guild Members:</b>'
    for timezone in timezones:
        persons = database.list_persons_by_timezone(timezone.timezone_float)

        local_datetime = util.float_to_local_datetime(timezone.timezone_float)

        part_of_day = util.get_part_of_day(local_datetime)

        response += '\n' + part_of_day + local_datetime.strftime("%H:%M") + ' (UTC' + timezone.timezone + ') ' + '\n'

        for person in persons:
            response += '- ' + person.name + '\n'

    bot.send_message(chat_id=update.message.chat_id, text=response, parse_mode=telegram.ParseMode.HTML)
