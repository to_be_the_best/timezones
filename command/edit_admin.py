#!/usr/bin/python

from typing import Any, Optional
import re
import telegram
import database
import edit_person
from restricted import restricted


@restricted
def edit_admin(bot: telegram.Bot, update: telegram.Update, args: Any):
    reply: Optional[telegram.Message] = update.message.reply_to_message

    if reply is None:
        bot.send_message(chat_id=update.message.chat_id, text='Reply is missing')
        return

    user: telegram.User = reply.from_user

    try:
        time_difference = args[0]
    except IndexError:
        bot.send_message(chat_id=update.message.chat_id, text='Parameters missing')
        return

    correct_time_difference = re.search(r'[-+]\d{1,2}(:\d{1,2})?|0', time_difference)

    if not correct_time_difference:
        bot.send_message(chat_id=update.message.chat_id,
                         text=r'Time difference does not match following regex [-+]\d{1,2}(:\d{1,2})?|0')
        return

    person = database.get_person(user.id)

    if person is None:
        bot.send_message(chat_id=update.message.chat_id, text='Person does not exists in database')
        return

    edit_person.edit_person(person, time_difference)

    bot.send_message(chat_id=update.message.chat_id, text='Edited @' + user.username + ' / ' + str(user.id))
