#!/usr/bin/python

from typing import Any, Optional
import re
import telegram
import database
import add_person
from restricted import restricted


@restricted
def add_admin(bot: telegram.Bot, update: telegram.Update, args: Any):
    reply: Optional[telegram.Message] = update.message.reply_to_message

    if reply is None:
        bot.send_message(chat_id=update.message.chat_id, text='Reply is missing')
        return

    user: telegram.User = reply.from_user

    try:
        time_difference = args[0]
    except IndexError:
        bot.send_message(chat_id=update.message.chat_id, text='Parameters missing')
        return

    correct_time_difference = re.search(r'[-+]\d{1,2}(:\d{1,2})?|0', time_difference)

    if not correct_time_difference:
        bot.send_message(chat_id=update.message.chat_id,
                         text=r'Time difference does not match following regex [-+]\d{1,2}(:\d{1,2})?|0')
        return

    name = ' '.join(args[1:])

    if database.get_person(user.id) is not None:
        bot.send_message(chat_id=update.message.chat_id, text='Person already exists in database')
        return

    add_person.add_person(user, time_difference, name)

    bot.send_message(chat_id=update.message.chat_id, text='Added @' + user.username + ' / ' + str(user.id))
