#!/usr/bin/python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session

# set echo to True to see logging information with sql statements
engine = create_engine('sqlite:///chatwars.db', echo=False)

ses = sessionmaker(bind=engine)
session: Session = ses()
