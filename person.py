#!/usr/bin/python

from sqlalchemy import Column, String, Float, Integer
from sqlalchemy.ext.declarative import declarative_base
from database_connection import engine

Base = declarative_base()


class Person(Base):
    __tablename__ = 'persons'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    username = Column(String)
    timezone = Column(String)
    timezone_float = Column(Float)


Base.metadata.create_all(engine)
